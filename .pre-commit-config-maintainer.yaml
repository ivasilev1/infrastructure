---
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.6.0
    hooks:
      - id: check-added-large-files
      - id: check-ast
      - id: check-docstring-first
      - id: check-executables-have-shebangs
      - id: check-merge-conflict
      - id: check-symlinks
      - id: check-yaml
        args:
          - "--allow-multiple-documents"
          # https://github.com/pre-commit/pre-commit-hooks/issues/273
          - "--unsafe"
      - id: detect-private-key
      - id: end-of-file-fixer
      - id: mixed-line-ending
      - id: no-commit-to-branch
        args:
          - "--branch"
          - "main"
      - id: trailing-whitespace

  - repo: https://github.com/ansible-community/ansible-lint.git
    rev: v24.2.2
    hooks:
      - id: ansible-lint
        args: ["--profile", "min"]

  - repo: https://github.com/Yelp/detect-secrets
    rev: v1.4.0
    hooks:
      - id: detect-secrets

  - repo: https://gitlab.com/devopshq/gitlab-ci-linter
    rev: v1.0.6
    hooks:
      - id: gitlab-ci-linter
        args:
          - "--project"
          - "testing-farm/infrastructure"

  # Disallow `.yml` files in the repo, i.e. assume usage of `.yaml` for YAML
  - repo: local
    hooks:
      - id: no-yml-extension
        name: no-yml-extension
        language: fail
        files: \.yml$
        entry: "bash -c 'find . -ina'"
        exclude: .gitlab-ci.yml

  # Terraform linters
  - repo: https://github.com/antonbabenko/pre-commit-terraform
    rev: v1.89.0
    hooks:
      - id: terraform_fmt
        args:
          - --args=-diff
      - id: terraform_tflint
      - id: terraform_tfsec
        exclude: |
          (?x)^(
            # well, tfsec does know nothing about terragrunt inputs :(
            terragrunt/environments/.*/server/ec2/.*
          )$
      - id: terragrunt_validate
        files: ^terragrunt/environments/
        exclude: |
          (?x)^(
            terragrunt/environments/.*/server/pods/.*|
            terragrunt/environments/.*/artemis/config/.*|
            terragrunt/environments/.*/worker/citool-config/.*|
            terragrunt/environments/.*/worker-local/citool-config/.*|
            terragrunt/environments/.*/worker-public/citool-config/.*|
            terragrunt/environments/.*/worker-redhat/citool-config/.*|
            terragrunt/environments/.*/worker-public/jobs/.*|
            terragrunt/environments/.*/worker-redhat/jobs/.*
          )$
      - id: terragrunt_fmt

  # Generate Public Ranch compose tests
  - repo: local
    hooks:
      - id: generate-public-compose-tests
        name: generate-public-compose-tests
        entry: make generate/public/tests/compose
        language: system
        pass_filenames: false
